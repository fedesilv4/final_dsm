import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Router, Scene } from  'react-native-router-flux';
import Welcome from './src/Welcome.js';
import Albums from './src/Albums.js'

// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Que pija</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
export default App = () =>(
  <Router>
      <Scene key="root">
          <Scene key="init" component= {Welcome}  title="Flickr" initial={true}/>
          <Scene key="albums" component= {Albums} tittle="Albums"/>
      </Scene>
  </Router>
)