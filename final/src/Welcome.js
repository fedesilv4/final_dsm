import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import { logo } from './../image/Flickr_logo.png'
import { Actions } from 'react-native-router-flux';

export default class Welcome extends React.Component
{
    render()
    {
        return(
            <View style={styles.container}>
                <Image 
                source={require( './../image/Flickr_logo.png')}
                style= {{width:230, height:70}}
                />
                <Text>Bienvenidos a Flickr!</Text>
                <Button
                raised
                icon={{name: 'cached'}}
                title='Ver albums!'
                onPress= { () => Actions.albums()}
                />
            </View>
        )
    }
}

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     backgroundColor: '#fff',
     alignItems: 'center',
     justifyContent: 'center',
   }
 });